FROM node:14
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --silent
COPY . . 
EXPOSE 3000
RUN npm run build

RUN npm i -g @microfrontends/serve

CMD ["npm", "run", "expose-prod"]

